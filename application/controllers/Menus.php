<?php
/**
 *
 */
class Menus extends CI_Controller
{

  function __construct()

  {
  parent::__construct();
  }
  //renderizacion de la vista que muestra los desayunos
  public function desayunos(){
    $this->load->view('header');
    $this->load->view('menus/desayunos');
    $this->load->view('footer');
  }
  public function almuerzos(){
    $this->load->view('header');
    $this->load->view('menus/almuerzos');
    $this->load->view('footer');
  }
  public function cenas(){
    $this->load->view('header');
    $this->load->view('menus/cenas');
    $this->load->view('footer');

  }
  public function contactos(){
    $this->load->view('header');
    $this->load->view('menus/contactos');
    $this->load->view('footer');

  }

}// no tocar el cierre de la clase



 ?>
