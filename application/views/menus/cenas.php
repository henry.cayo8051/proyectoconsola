<h1 class="text-center"; style="color:green"> Meriendas Esquisitas</h1>

<div class="continer">
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
    <img src="<?php echo base_url();?>/assets/images/m1.jpg"  width="450" height="300" alt="">
      <div class="caption">
        <h3 class="text-center">Sopa de Pollo</h3>
        <p class="text-center">$4.50 </p>
        <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
    <img src="<?php echo base_url();?>/assets/images/m2.png"  width="450" height="300" alt="">
      <div class="caption">
        <h3 class="text-center">Sancocho de verde</h3>
        <p class="text-center">$4.50 </p>
        <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
    <img src="<?php echo base_url();?>/assets/images/m3.jpg"  width="450" height="300" alt="">
      <div class="caption">
        <h3 class="text-center">Pinchos mixtos</h3>
        <p class="text-center">$4.50 </p>
        <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
</div>
</div>
<div class="continer">
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
    <img src="<?php echo base_url();?>/assets/images/m4.jpg"  width="450" height="300" alt="">
      <div class="caption">
        <h3 class="text-center">Hamburgesa</h3>
        <p class="text-center">$4.50 </p>
        <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
    <img src="<?php echo base_url();?>/assets/images/m5.jpg"  width="450" height="300" alt="">
      <div class="caption">
        <h3 class="text-center">Seco de Carne</h3>
        <p class="text-center">$4.50 </p>
        <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
    <img src="<?php echo base_url();?>/assets/images/m6.jpg"  width="450" height="300" alt="">
      <div class="caption">
        <h3 class="text-center">Sopa de Verduras</h3>
        <p class="text-center">$4.50 </p>
        <p class="text-center" ><a href="#" class="btn btn-primary" role="button">Ordenar</a> <a href="#" class="btn btn-default" role="button">Cancelar</a></p>
      </div>
    </div>
  </div>
</div>
</div>
