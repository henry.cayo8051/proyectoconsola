<h1 class="text-center"; style="color:green"> BIENVENIDOS AL RESTAURANTE UTC</h1>
<center><img src="<?php echo base_url();?>/assets/images/portada.jpg"  width="650" height="500" alt="">
</center>
<br>
<br>
<div class="alert alert-success" role="alert">
  <h4 class="alert-heading">MISIÓN</h4>
  <p>Somos una empresa dedicada a brindar momentos inolvidables y servicios gastronómicos de alta
calidad; ponemos todo nuestro “amor” y máximo empeño en beneficio de nuestros clientes;
desarrollamos nuestro servicio a partir de los talentos y los valores de nuestros
colaboradores, somos una empresa que día a día lucha por desarrollar mejores condiciones
laborales y un mejor nivel de vida para nuestros colaboradores y sus familias, en beneficio de
la organización.</p>
</div>
<br>
<div class="alert alert-success" role="alert">
  <h4 class="alert-heading">VISIÓN</h4>
  <p>Ser reconocidos por brindar a nuestros clientes sensaciones agradables y momentos felices.
Posicionarnos en el corazón de las familias palmiranas y de todos los que nos visitan.
Contribuir y aportar nuestro granito de arena, para generar una colombia feliz y en paz; que
brinde un mejor futuro a nuestras próximas generaciones.</p>
  
</div>
<div class="container" style="color: forestgreen">
        <h2><b>Ubicanos en:</b></h2>
        <div class="row">
            <div class="col-md-4">
                <table class="table table-bordered table-striped table-hover">
                    <br>
                    <tr>
                        <th class="text-right">DIRECIÓN:</th>
                        <td>39J8+RRJ, Av. Simón Rodríguez, Latacunga</td>
                    </tr>
                    <tr>
                        <th class="text-right">HORARIO DE ATENCIÓN:</th>
                        <td> De 09:00 am Hasta 10:00 pm</td>
                    </tr>
                    <tr>
                        <th class="text-right">TELÉFONO:</th>
                        <td>(03) 281-0296</td>
                    </tr>
                    <tr>
                        <th class="text-right">UBICAIÓN:</th>
                        <td>LATACUNGA-ECUADOR</td>
                    </tr>
                    <tr>
                        <th class="text-right">EMAIL:</th>
                        <td>restaurante.utc@gmail.com</td>
                    </tr>
                    
                </table>
            </div>
                <div class="col-md-7">
                    <div id="mapaDireccion" style="100%; height:350px;"></div>
                </div>
        </div>
        
        <script type="text/javascript">
            function initMap(){
                var coordenadaCentral=
                new google.maps.LatLng(-0.9155596917144397, -78.63296837433572);
                var mapa1=new google.maps.Map(document.getElementById("mapaDireccion"),
                {
                    center:coordenadaCentral,
                    zoom:15,
                    mapTypeId:google.maps.MapTypeId.ROADMAP
                }
                );
            }
        </script>

    </div>